package com.HealthInsurance;

import java.util.HashMap;
import java.util.Map;
//main
public class HealthInsuranceMain {
	
	public static void main(String args []){
		
		String name = "Norman Gomes";
		int age = 34;
		String gender = "Male";
		
		Map<String,String> currentHealth = new HashMap<>();
		currentHealth.put("Hypertension", "No");
		currentHealth.put("Blood pressure", "No");
		currentHealth.put("Blood Sugar", "No");
		currentHealth.put("Overweight", "Yes");
		
		Map<String,String> habits = new HashMap<>();
		habits.put("Smoking", "No");
		habits.put("Alcohol", "Yes");
		habits.put("Daily exercise", "Yes");
		habits.put("Drugs", "No");
		
		HealthQuoteGenerator quote = new HealthQuoteGenerator();
		double healthQuote= quote.generateHealthQuote(age, gender, currentHealth, habits);
		System.out.println("Hi "+ name +"your health quote is "+Math.ceil(healthQuote));
	}

}
