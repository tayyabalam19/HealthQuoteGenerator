package com.HealthInsurance;

import java.util.Map;

public class HealthQuoteGenerator {

	private double basePremium = 5000;
	private double actualPremium = 0;

	public double generateHealthQuote(int age, String gender, Map<String, String> currentHealth,
			Map<String, String> habits) {

		//age based calculation
		actualPremium = getValidateAgeAmount(age);

		//gender based calculation
		if (gender != null && gender.equalsIgnoreCase("Male")) {
			actualPremium = actualPremium + 0.02 * actualPremium;
		}
 
		//health based calculation
		if (currentHealth != null) {
			if (currentHealth.get("Hypertension").equalsIgnoreCase("Yes")) {
				actualPremium = actualPremium + 0.01 * actualPremium;
			}
			if (currentHealth.get("Blood pressure").equalsIgnoreCase("Yes")) {
				actualPremium = actualPremium + 0.01 * actualPremium;
			}
			if (currentHealth.get("Blood Sugar").equalsIgnoreCase("Yes")) {
				actualPremium = actualPremium + 0.01 * actualPremium;
			}
			if (currentHealth.get("Overweight").equalsIgnoreCase("Yes")) {
				actualPremium = actualPremium + 0.01 * actualPremium;
			}
		}

		//habits based calculation
		if (habits != null) {
			if (habits.get("Smoking").equalsIgnoreCase("Yes")) {
				actualPremium = actualPremium + 0.03 * actualPremium;
			}
			if (habits.get("Alcohol").equalsIgnoreCase("Yes")) {
				actualPremium = actualPremium + 0.03 * actualPremium;
			}
			if (habits.get("Daily exercise").equalsIgnoreCase("Yes")) {
				actualPremium = actualPremium - 0.03 * actualPremium;
			}
			if (habits.get("Drugs").equalsIgnoreCase("Yes")) {
				actualPremium = actualPremium + 0.03 * actualPremium;
			}
		}

		return actualPremium;
	}

	private double getValidateAgeAmount(int age) {
		double amount = 0;
		if (age >= 18 && age < 25) {
			amount = basePremium + 0.1 * basePremium;
		} else if (age >= 25 && age < 30) {
			amount = basePremium + 0.1 * basePremium;
			amount = amount + 0.1 * actualPremium;
		} else if (age >= 30 && age < 35) {
			amount = basePremium + 0.1 * basePremium;
			amount = amount + 0.1 * amount;
			amount = amount + 0.1 * amount;
		} else if (age >= 35 && age < 40) {
			amount = basePremium + 0.1 * basePremium;
			amount = amount + 0.1 * amount;
			amount = amount + 0.1 * amount;
			amount = amount + 0.1 * amount;
		} else if (age > 40) {
			amount = basePremium + 0.1 * basePremium;
			amount = amount + 0.1 * amount;
			amount = amount + 0.1 * amount;
			amount = amount + 0.1 * amount;
			amount = amount + 0.2 * amount;
		}

		return amount;
	}

}
