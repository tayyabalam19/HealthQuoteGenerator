package com.HealthInsuranceTest;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import com.HealthInsurance.HealthQuoteGenerator;

public class HealthInsuranceTest {
	
	@Test
	public void HealthQuoteGeneratorTest(){
		
		String name = "Norman Gomes";
		int age = 34;
		String gender = "Male";
		
		Map<String,String> currentHealth = new HashMap<String,String>();
		currentHealth.put("Hypertension", "No");
		currentHealth.put("Blood pressure", "No");
		currentHealth.put("Blood Sugar", "No");
		currentHealth.put("Overweight", "Yes");
		
		Map<String,String> habits = new HashMap<String,String>();
		habits.put("Smoking", "No");
		habits.put("Alcohol", "Yes");
		habits.put("Daily exercise", "Yes");
		habits.put("Drugs", "No");
		
		HealthQuoteGenerator quote = new HealthQuoteGenerator();
		double healthQuote= quote.generateHealthQuote(age, gender, currentHealth, habits);
	
		Assert.assertTrue(Math.ceil(healthQuote) == 6850);
		
		
	}

}
